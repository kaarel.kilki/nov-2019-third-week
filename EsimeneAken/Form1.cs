﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Form1 : Form
    {
        List<Tulemus> Tulemused = null;
        string allItems = "X";
        string filename = "..\\..\\spordipäeva protokoll.txt";
        public Form1()
        {
            InitializeComponent();
        }

        

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
                
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.IO.File.ReadAllLines(filename)
                .Skip(1)
                .Select(x => x.Split(','))
                .Select(x => new Tulemus { 
                    Nimi = x[0],
                    Distants = double.TryParse(x[1], out double d) ? d : 0,
                    Aeg = double.TryParse(x[2], out double a) ? a : 0,
                })
                .ToList();
            this.dataGridView1.DataSource = Tulemused;
            this.dataGridView1.Columns[3].DefaultCellStyle.Format = "F2";
            this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            this.comboBox1.DataSource =
                Tulemused.Select(x => x.Nimi).Distinct()
                .Union(new string[] { allItems})
                .OrderBy(x => x == allItems ? "" : x)
                .ToList();

        }

        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string nimi = this.comboBox1.SelectedItem.ToString();
            this.dataGridView1.DataSource = Tulemused
                .Where(x => x.Nimi == nimi || nimi == allItems)
                .ToList()
                ;
            this.dataGridView1.Columns[3].DefaultCellStyle.Format = "F2";
            this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllLines(filename,
                (new string[] { "Nimi,Distants,Aeg"})
                .Union(
                    Tulemused
                    .Select(x => $"{x.Nimi},{x.Distants},{x.Aeg}")
                )
                
                );
        }

    }
   
}
