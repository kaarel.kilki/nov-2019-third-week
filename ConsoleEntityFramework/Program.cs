﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEntityFramework
{
    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";
    }
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();

            #region kommenteerin välja
            //ne.Database.Log = Console.WriteLine;    // trükib välja SQL-i

            //foreach (var p in ne.Products
            //    .Select(x => new { x.UnitPrice, x.ProductName })
            //    .Where(x => x.UnitPrice > 100)
            //    .OrderBy(x => x.UnitPrice)
            //    )
            //    Console.WriteLine($"{p.ProductName}");

            //Console.WriteLine(
            //    ne.Categories.Find(8).CategoryName  // Find oskab otsida, kui on tegemist Key Propertyga
            //    );
            //ne.Categories.Find(8).CategoryName = "Seafood";
            //Console.WriteLine(
            //    ne.Categories.Find(8).CategoryName
            //    );
            //ne.SaveChanges();
            ////Product uus = new Product { ProductName = "Mängukaru", UnitPrice = 10, CategoryID = 8 };
            ////ne.Products.Add(uus);
            ////ne.SaveChanges();

            ////ne.Categories.Find(1).Products.Add(new Product { ProductName = "lutsukomm", UnitPrice = 0.5M });      // lisab esimesse kategooriasse
            ////ne.SaveChanges();

            ////foreach (var p in ne.Products.Where(x => x.ProductName.StartsWith("Mängu"))) ne.Products.Remove(p);   // saab kustutada kategooriast objekte
            ////ne.SaveChanges(); 
            #endregion

            foreach (var e in ne.Employees)
                Console.WriteLine($"{e.FullName} ülemus on {e.Manager?.FullName}");
            
        }
    }
}
