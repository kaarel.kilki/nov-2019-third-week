﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenEntity
{
    public partial class Form1 : Form
    {
        // kui static - siis kõigil akendel ühine db
        // kui mitte static - siis igal aknal oma db
        NorthwindEntities db = new NorthwindEntities();
        public Form1()                  // constructor, kuna nimi klapib classi nimega ja puudub "void"
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)  // meetod, sest omab "void"i (kui olks "int", "string" vms, siis oleks funktsioon)
        {
            this.comboBox1.DataSource = db.Categories
                .Select(x => x.CategoryName)
                .ToList();
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Categories.Where(c => c.CategoryName == this.comboBox1.SelectedItem.ToString())
                .SingleOrDefault().Products
                //.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.UnitsInStock})
                .ToList();
            this.dataGridView1.Columns["CategoryId"].Visible = false;
            this.dataGridView1.Columns["SupplierId"].Visible = false;
            this.dataGridView1.Columns["Category"].Visible = false;
            this.dataGridView1.Columns["unitPrice"].DefaultCellStyle.Format = "F2";
        }
        private void clickToSave(object sender, EventArgs e)
        {
            db.SaveChanges();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch(
            MessageBox.Show("Kas salvestan?", "Küsimus", MessageBoxButtons.YesNoCancel)
            )
            {
                case DialogResult.Yes:
                    db.SaveChanges();
                    break;
                case DialogResult.No:
                    break;
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            (new Form1()).Show();
        }
    }
}
