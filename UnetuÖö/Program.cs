﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnetuÖö
{
    class Program
    {
        
        static void Main(string[] args)
        {
            // 1. kuidas saada uus Jänes                        new
            // 2. kuidas saada Jänes teiste hulka               ???
            // 3. kuidas leida Jänes                            ???
            // 4. kuidas leida kõik jänesed                     foreach?
            // 5. kuidas saada Jänes teiste hulgast minema      ???
            // 1
            Jänes juta = new Jänes { Id = 7, Nimi = "Juta" };
            // 2
            juta.Add();
            // 3
            Jänes kalle = Jänes.Find(3);
            // 4
            foreach (var j in Jänes.Jänesed) Console.WriteLine($"{j.Id} {j.Nimi}");
            // 5
            Jänes kaabu = new Jänes { Id = 1, Nimi = "Herbert" };
            kaabu.Add();
            foreach (var j in Jänes.Jänesed) Console.WriteLine($"{j.Id} {j.Nimi}");

            

            kalle?.Remove();
            if (kalle != null) kalle.Remove();

            Jänes.Save();

        }
    }
    class Jänes
    {
        const string FILENAME = "..\\..\\jänesed.txt";
        static Jänes()

        {
            // Loeme failist jänesed

            try
            {
                _Jänesed =
                    System.IO.File.ReadAllLines(FILENAME)
                        .Select(x => x.Split(','))
                        .Select(x => new Jänes { Id = int.Parse(x[0]), Nimi = x[1] })
                        .ToDictionary(x => x.Id);
            }
            catch (Exception)
            {

            }
        }
        public static void Save()
        {
            // Kirjutame jänesed faili
            System.IO.File.WriteAllLines(FILENAME,
                _Jänesed.Values.Select(x => $"{x.Id},{x.Nimi}")
                );
        }
        // 1
        static Dictionary<int, Jänes> _Jänesed = new Dictionary<int, Jänes>();
        public int Id;
        public string Nimi;

        public static IEnumerable<Jänes> Jänesed => _Jänesed.Values;
        // 2
        public void Add()
        {
            if (!_Jänesed.ContainsKey(this.Id))
                _Jänesed.Add(this.Id, this);
        }
        // 3
        public static Jänes Find(int id) => _Jänesed.ContainsKey(id) ? _Jänesed[id] : null;
        // 5
        public void Remove() => _Jänesed.Remove(this.Id);
    }

}
