﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeineMVC2.Models;
using System.ComponentModel.DataAnnotations;

namespace TeineMVC2.Controllers
{
    public class HobusedController : Controller
    {
        // GET: Hobused
        public ActionResult Index()
        {
            return View(Hobune.Hobused);
        }

        // GET: Hobused/Details/5
        public ActionResult Details(int id)
        {
            return View(Hobune.Find(id));
        }

        // GET: Hobused/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hobused/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                new Hobune
                {
                    Nimi = collection["Nimi"].ToString(),
                    Värv = collection["Värv"].ToString(),
                };

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hobused/Edit/5
        public ActionResult Edit(int id)
        {

            return View(Hobune.Find(id));
        }

        // POST: Hobused/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Hobune hobune = Hobune.Find(id);
            if (hobune == null) return RedirectToAction("Index");
            try
            {
                // TODO: Add update logic here
                hobune.Nimi = collection["Nimi"].ToString();
                hobune.Värv = collection["Värv"].ToString();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hobused/Delete/5
        public ActionResult Delete(int id)
        {
            Hobune kes = Hobune.Find(id);
            if (kes == null) RedirectToAction("Index");
            return View(kes);
        }

        // POST: Hobused/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Hobune kes = Hobune.Find(id);
                kes?.Remove();
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
