﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TeineMVC2.Models
{
    public class Hobune
    {
        static Dictionary<int, Hobune> _Hobused = 
            new Dictionary<int, Hobune>()
            ;
        static Hobune()     // static constructor
        {
            new Hobune { Nimi = "Kevin", Värv = "kõrb" };
            new Hobune { Nimi = "Verde", Värv = "kõrb" };
        }
        static int nr = 0;
        public int Id { get; set; } //= ++nr;
        public string Nimi { get; set; }
        public string Värv { get; set; }
        public Hobune() { _Hobused.Add(this.Id = ++nr, this); }
        public static Hobune Find(int id) =>
            _Hobused.ContainsKey(id) ? _Hobused[id] : null;
        public static IEnumerable<Hobune> Hobused => _Hobused.Values;
        public void Remove() => _Hobused.Remove(this.Id);
    }
}