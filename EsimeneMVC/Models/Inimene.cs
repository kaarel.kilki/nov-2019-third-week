﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EsimeneMVC.Models
{
    public class Inimene
    {
        static int nr = 0;
        public static List<Inimene> Inimesed = 
            new List<Inimene>
            {
                new Inimene {Nimi = "Henn", Vanus = 64},
                new Inimene {Nimi = "Ants", Vanus = 40},
                new Inimene {Nimi = "Peeter", Vanus = 33},
            };
        public int Id { get; } = ++nr;
        [Display(Name = "Person name")]
        public string Nimi { get; set; }
        [Display(Name = "Person age")]
        public int Vanus { get; set; }
        public static Inimene Find(int id)
            => Inimesed.Where(x => x.Id == id).SingleOrDefault();

    }
}