﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenEntityIse
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = db.Customers
                .Select(x => x.Country)
                .Distinct()
                .ToList()
                ;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = db.Customers
                .Where(c => c.Country == this.comboBox1.SelectedItem.ToString())
                .ToList();
            this.dataGridView1.Columns["Fax"].Visible = false;
            this.dataGridView1.Columns["Phone"].Visible = false;
            this.dataGridView1.Columns["PostalCode"].Visible = false;
            this.dataGridView1.Columns["Region"].Visible = false;
            this.dataGridView1.Columns["ContactName"].Visible = false;
            this.dataGridView1.Columns["ContactTitle"].Visible = false;


        }
    }
}
