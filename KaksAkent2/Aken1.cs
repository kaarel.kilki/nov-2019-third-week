﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaksAkent2
{
    public partial class Aken1 : Form
    {
        NorthwindEntities ne = new NorthwindEntities();
        public Aken1()
        {
            InitializeComponent();
        }

        private void Aken1_Load(object sender, EventArgs e)
        {
            this.listBox1.DataSource = ne.Categories
                .Select(x => x.CategoryName)
                .ToList()
                ;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            (new Teine {CategoryName = this.listBox1.Text }).Show();
        }
    }
}
