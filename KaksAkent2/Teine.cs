﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaksAkent2
{
    public partial class Teine : Form
    {
        public string CategoryName;
        public Teine()
        {
            InitializeComponent();
        }

        private void Teine_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = ne
                .Categories.Where(x => x.CategoryName == this.CategoryName)
                .SingleOrDefault()
                .Products
                .ToList();
        }
    }
}
