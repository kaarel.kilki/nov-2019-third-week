﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaksAkent
{
    public partial class Aken2 : Form
    {
        public string Teade;
        public string CategoryName;
        public Aken2()
        {
            InitializeComponent();
            this.label1.Text = Teade;
        }

        private void Aken2_Load(object sender, EventArgs e)
        {
            this.label1.Text = Teade;
            NorthwindEntities ne = new NorthwindEntities();
            this.dataGridView1.DataSource = ne
                .Categories.Where(x => x.CategoryName == this.CategoryName)
                .SingleOrDefault()
                .Products
                .ToList();
        }
    }
}
